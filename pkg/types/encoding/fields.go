package encoding

type Accessor[T any] interface {
	IsEmpty(T) bool
	CopyTo(dst, src T)
	Equal(v, u T) bool
	WriteTo(w *Writer, n uint, v T)
	ReadFrom(r *Reader, n uint, v T)
}

type IntField[T any] func(v T) *int64
type StringField[T any] func(v T) *string

func (f IntField[T]) IsEmpty(v T) bool    { return *f(v) == 0 }
func (f StringField[T]) IsEmpty(v T) bool { return *f(v) == "" }

func (f IntField[T]) CopyTo(dst, src T)    { *f(dst) = *f(src) }
func (f StringField[T]) CopyTo(dst, src T) { *f(dst) = *f(src) }

func (f IntField[T]) Equal(v, u T) bool    { return *f(v) == *f(u) }
func (f StringField[T]) Equal(v, u T) bool { return *f(v) == *f(u) }

func (f IntField[T]) WriteTo(w *Writer, n uint, v T)    { w.WriteInt(n, *f(v)) }
func (f StringField[T]) WriteTo(w *Writer, n uint, v T) { w.WriteString(n, *f(v)) }

func (f IntField[T]) ReadFrom(r *Reader, n uint, v T) {
	if x, ok := r.ReadInt(n); ok {
		*f(v) = x
	}
}

func (f StringField[T]) ReadFrom(r *Reader, n uint, v T) {
	if x, ok := r.ReadString(n); ok {
		*f(v) = x
	}
}

type Field[T any] struct {
	Accessor[T]
	Name      string
	OmitEmpty bool
	Required  bool
}

func (f *Field[T]) WriteTo(w *Writer, n uint, v T) {
	if f.OmitEmpty && f.Accessor.IsEmpty(v) {
		return
	}
	f.Accessor.WriteTo(w, n, v)
}
