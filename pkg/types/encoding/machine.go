package encoding

import (
	"bytes"
	"errors"
	"io"
	"strings"
)

type Machine[T any] struct {
	Fields    []*Field[T]
	ExtraData func(v T) *[]byte
	Seen      func(v T) *[]bool
}

func (m *Machine[T]) WriteTo(w *Writer, v T) {
	for i, f := range m.Fields {
		f.WriteTo(w, uint(i+1), v)
	}
}

func (m *Machine[T]) MarshalBinary(v T) ([]byte, error) {
	buffer := new(bytes.Buffer)
	writer := NewWriter(buffer)

	m.WriteTo(writer, v)

	_, _, err := writer.Reset(nil) // TODO field names
	if err != nil {
		return nil, Error{E: err}
	}
	buffer.Write(*m.ExtraData(v))
	return buffer.Bytes(), nil
}

func (m *Machine[T]) ReadFrom(r *Reader, v T) {
	for i, f := range m.Fields {
		f.ReadFrom(r, uint(i+1), v)
	}
}

func (m *Machine[T]) Unmarshal(b []byte, v T) error {
	return m.UnmarshalFrom(bytes.NewReader(b), v)
}

func (m *Machine[T]) UnmarshalFrom(rd io.Reader, v T) error {
	reader := NewReader(rd)

	m.ReadFrom(reader, v)

	var err error
	*m.Seen(v), err = reader.Reset(nil) // TODO field names
	if err != nil {
		return Error{E: err}
	}
	*m.ExtraData(v), err = reader.ReadAll()
	if err != nil {
		return Error{E: err}
	}
	return nil
}

func (m *Machine[T]) IsValid(v T) error {
	var errs []string

	seen := *m.Seen(v)

	for i, f := range m.Fields {
		if len(seen) > i && !seen[i] {
			errs = append(errs, "field "+f.Name+" is missing")
		} else if f.IsEmpty(v) {
			errs = append(errs, "field "+f.Name+" is not set")
		}
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

func (m *Machine[T]) Equal(v, u T) bool {
	// Check for reference equality
	if any(v) == any(u) {
		return true
	}

	// Check for nil
	var z T
	if any(v) == any(z) || any(u) == any(z) {
		return false
	}

	// Check fields
	for _, f := range m.Fields {
		if !f.Equal(v, u) {
			return false
		}
	}
	return true
}

func Copy[T any, PT interface{ *T }](m *Machine[PT], v PT) PT {
	u := PT(new(T))
	for _, f := range m.Fields {
		f.CopyTo(u, v)
	}
	return u
}
