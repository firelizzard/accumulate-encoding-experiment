package foo

import (
	"io"

	"gitlab.com/accumulatenetwork/core/encoding/pkg/types/encoding"
)

type Foo struct {
	fieldsSet []bool
	A         int64  `json:"a,omitempty" form:"a" query:"a" validate:"required"`
	B         string `json:"b,omitempty" form:"b" query:"b" validate:"required"`
	extraData []byte
}

var machine_Foo = &encoding.Machine[*Foo]{
	Fields: []*encoding.Field[*Foo]{
		{Name: "A", OmitEmpty: true, Required: true, Accessor: encoding.IntField[*Foo](func(v *Foo) *int64 { return &v.A })},
		{Name: "B", OmitEmpty: true, Required: true, Accessor: encoding.StringField[*Foo](func(v *Foo) *string { return &v.B })},
	},
	ExtraData: func(v *Foo) *[]byte { return &v.extraData },
	Seen:      func(v *Foo) *[]bool { return &v.fieldsSet },
}

func (v *Foo) IsValid() error                         { return machine_Foo.IsValid(v) }
func (v *Foo) Copy() *Foo                             { return encoding.Copy(machine_Foo, v) }
func (v *Foo) CopyAsInterface() interface{}           { return v.Copy() }
func (v *Foo) Equal(u *Foo) bool                      { return machine_Foo.Equal(v, u) }
func (v *Foo) MarshalBinary() ([]byte, error)         { return machine_Foo.MarshalBinary(v) }
func (v *Foo) UnmarshalBinary(data []byte) error      { return machine_Foo.Unmarshal(data, v) }
func (v *Foo) UnmarshalBinaryFrom(rd io.Reader) error { return machine_Foo.UnmarshalFrom(rd, v) }
