package foo

import (
	"bytes"
	"errors"
	"io"
	"strings"

	"gitlab.com/accumulatenetwork/core/encoding/pkg/types/encoding"
)

type Foo struct {
	fieldsSet []bool
	A         int64  `json:"a,omitempty" form:"a" query:"a" validate:"required"`
	B         string `json:"b,omitempty" form:"b" query:"b" validate:"required"`
	extraData []byte
}

func (v *Foo) Copy() *Foo {
	u := new(Foo)

	u.A = v.A
	u.B = v.B

	return u
}

func (v *Foo) CopyAsInterface() interface{} { return v.Copy() }

func (v *Foo) Equal(u *Foo) bool {
	if !(v.A == u.A) {
		return false
	}
	if !(v.B == u.B) {
		return false
	}

	return true
}

var fieldNames_Foo = []string{
	1: "A",
	2: "B",
}

func (v *Foo) MarshalBinary() ([]byte, error) {
	buffer := new(bytes.Buffer)
	writer := encoding.NewWriter(buffer)

	if !(v.A == 0) {
		writer.WriteInt(1, v.A)
	}
	if !(len(v.B) == 0) {
		writer.WriteString(2, v.B)
	}

	_, _, err := writer.Reset(fieldNames_Foo)
	if err != nil {
		return nil, encoding.Error{E: err}
	}
	buffer.Write(v.extraData)
	return buffer.Bytes(), nil
}

func (v *Foo) IsValid() error {
	var errs []string

	if len(v.fieldsSet) > 0 && !v.fieldsSet[0] {
		errs = append(errs, "field A is missing")
	} else if v.A == 0 {
		errs = append(errs, "field A is not set")
	}
	if len(v.fieldsSet) > 1 && !v.fieldsSet[1] {
		errs = append(errs, "field B is missing")
	} else if len(v.B) == 0 {
		errs = append(errs, "field B is not set")
	}

	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errors.New(errs[0])
	default:
		return errors.New(strings.Join(errs, "; "))
	}
}

func (v *Foo) UnmarshalBinary(data []byte) error {
	return v.UnmarshalBinaryFrom(bytes.NewReader(data))
}

func (v *Foo) UnmarshalBinaryFrom(rd io.Reader) error {
	reader := encoding.NewReader(rd)

	if x, ok := reader.ReadInt(1); ok {
		v.A = x
	}
	if x, ok := reader.ReadString(2); ok {
		v.B = x
	}

	seen, err := reader.Reset(fieldNames_Foo)
	if err != nil {
		return encoding.Error{E: err}
	}
	v.fieldsSet = seen
	v.extraData, err = reader.ReadAll()
	if err != nil {
		return encoding.Error{E: err}
	}
	return nil
}
